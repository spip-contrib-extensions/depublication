<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_art_depub' => 'Ajouter cette dépublication',

	// I
	'icone_creer_art_depub' => 'Créer une dépublication',
	'icone_modifier_art_depub' => 'Modifier cette dépublication',
	'info_1_art_depub' => 'Une dépublication',
	'info_articles_depublication_auteur' => 'Les dépublications de cet auteur',
	'info_aucun_art_depub' => 'Aucune dépublication',
	'info_nb_articles_depublication' => '@nb@ dépublications',

	// L
	'label_depublication' => 'depublication',
	'label_id_article' => 'id_article',
	'label_maj' => 'maj',
	'label_statut' => 'statut',

	// R
	'retirer_lien_art_depub' => 'Retirer cette dépublication',
	'retirer_tous_liens_articles_depublication' => 'Retirer toutes les dépublications',

	// T
	'texte_ajouter_art_depub' => 'Ajouter une dépublication',
	'texte_changer_statut_art_depub' => 'Cette dépublication est :',
	'texte_creer_associer_art_depub' => 'Créer et associer une dépublication',
	'titre_art_depub' => 'Dépublication',
	'titre_articles_depublication' => 'Dépublications',
	'titre_articles_depublication_rubrique' => 'Dépublications de la rubrique',
	'titre_langue_art_depub' => 'Langue de cette dépublication',
	'titre_logo_art_depub' => 'Logo de cette dépublication',
);

?>