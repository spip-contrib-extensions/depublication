<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'depublication2_description' => 'Test en live de la fabrique pour adaptation de depublication',
	'depublication2_nom' => 'Dépublication 2',
	'depublication2_slogan' => 'plugin de test par Djo',
);

?>