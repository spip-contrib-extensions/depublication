<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     Dépublication 2
 * @copyright  2013
 * @author     Web
 * @licence    GNU/GPL
 * @package    SPIP\Depublication2\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');

?>